import sys
import getopt
import os.path
import numpy
import matplotlib.pylab as plt

white = 0
black = 1
is_out = False
result = 0


def travel(line, col):
    global is_out
    if line < 0 or col < 0:
        is_out = True
        return

    try:
        current = image[line][col]
    except IndexError:
        is_out = True
        return

    if current != black:
        return

    image[line][col] = result+1

    travel(line - 1, col)
    travel(line, col - 1)
    travel(line + 1, col)
    travel(line, col + 1)

    if eight_connectivity:
        travel(line-1, col-1)
        travel(line-1, col+1)
        travel(line+1, col-1)
        travel(line+1, col+1)


def travel_image(hole):
    global is_out
    global result
    for i in range(0, len(image)):
        for j in range(0, len(image[i])):
            if image[i][j] == black:
                result += 1
                travel(i, j)
                if is_out and hole:
                    result -= 1
                    is_out = False
    return result


def read_input_file(filename):
    lines = [line.rstrip('\n') for line in open(filename)]
    image = []
    for line in lines:
        image.append([int(cell) for cell in line])
    return image


def print_image():
    matrix = numpy.matrix(image)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_aspect('equal')
    plt.imshow(matrix, interpolation='nearest', cmap=plt.cm.YlGnBu)
    plt.show()


def print_help():
    print('main.py -i <inputfile>')
    print('You can use the eight connectivity by adding option -e')


def main():
    global image
    global eight_connectivity
    global result
    global white, black
    input_file = ''
    eight_connectivity = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:e", ["input="])
    except getopt.GetoptError:
        print_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
            sys.exit()
        elif opt in ("-i", "--ifile"):
            print("Ping", opt, arg)
            input_file = arg
        elif opt == '-e':
            eight_connectivity = not eight_connectivity

    if input_file == '':
        print("You must specified an input file!")
        print_help()
        sys.exit(2)

    if not os.path.isfile(input_file):
        print_help()
        sys.exit(2)

    print("Loading file: ", input_file)
    image = read_input_file(input_file)
    print_image()

    comp = travel_image(False)
    print("Comp. conx: ", comp)
    print_image()

    result = 0
    image = read_input_file(input_file)
    eight_connectivity = not eight_connectivity
    [white, black] = [black, white]
    hole = travel_image(True)
    print("Hole(s): ", hole)
    print("Euler:", comp - hole)


if __name__ == "__main__":
    main()
