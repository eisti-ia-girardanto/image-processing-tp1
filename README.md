# TP1 Image processing

Author: Antoine Girard

## Dependencies

- Python 3
- Virtualenv
- Numpy
- Matplotlib

## Getting started

### Start virtual env & install dependencies

```bash
source venv/bin/activate
pip install -r requirements.txt
```

### Start script

To use this script you need an input file. This input file must be composed only
with 0 and 1, like this: 

```
000000
011110
100001
100001
100001
011110
```

This file represent a binary image.

```bash
python main.py -i inputs/a
```

By default the script use 4-connectivity. If you want to use 
8-connectivity you can add `-e` option.

```bash
python main.py -i inputs/a -e
 ```
 
The script :

- Load file 
- Plot image
- Compute connectivity
- Plot image with components
- Compute holes number
- Compute euler number